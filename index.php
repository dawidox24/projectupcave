<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('dashboard', 'DefaultController');

Router::get('login', 'DefaultController');
Router::post('login', 'SecurityController');

Router::post('register', 'SecurityController');

Router::get('chat', 'DefaultController');
Router::get('my_friends', 'DefaultController');
Router::get('meetups', 'DefaultController');
Router::get('my_account', 'DefaultController');
Router::get('logout', 'DefaultController');

session_start();

Router::run($path);