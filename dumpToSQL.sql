--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5 (Ubuntu 13.5-2.pgdg20.04+1)
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: ybwkcejtdibexn
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO ybwkcejtdibexn;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: ybwkcejtdibexn
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: attributes; Type: TABLE; Schema: public; Owner: ybwkcejtdibexn
--

CREATE TABLE public.attributes (
    id integer NOT NULL,
    id_type integer,
    name character varying(30)
);


ALTER TABLE public.attributes OWNER TO ybwkcejtdibexn;

--
-- Name: attribute_list_id_seq; Type: SEQUENCE; Schema: public; Owner: ybwkcejtdibexn
--

CREATE SEQUENCE public.attribute_list_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attribute_list_id_seq OWNER TO ybwkcejtdibexn;

--
-- Name: attribute_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ybwkcejtdibexn
--

ALTER SEQUENCE public.attribute_list_id_seq OWNED BY public.attributes.id;


--
-- Name: attribute_type; Type: TABLE; Schema: public; Owner: ybwkcejtdibexn
--

CREATE TABLE public.attribute_type (
    id integer NOT NULL,
    name character varying(60) NOT NULL
);


ALTER TABLE public.attribute_type OWNER TO ybwkcejtdibexn;

--
-- Name: attribute_type_id_seq; Type: SEQUENCE; Schema: public; Owner: ybwkcejtdibexn
--

CREATE SEQUENCE public.attribute_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attribute_type_id_seq OWNER TO ybwkcejtdibexn;

--
-- Name: attribute_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ybwkcejtdibexn
--

ALTER SEQUENCE public.attribute_type_id_seq OWNED BY public.attribute_type.id;


--
-- Name: user_attributes; Type: TABLE; Schema: public; Owner: ybwkcejtdibexn
--

CREATE TABLE public.user_attributes (
    id_user integer NOT NULL,
    id_attribute integer NOT NULL
);


ALTER TABLE public.user_attributes OWNER TO ybwkcejtdibexn;

--
-- Name: users; Type: TABLE; Schema: public; Owner: ybwkcejtdibexn
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    surname character varying(100) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    avatar_url character varying(255) GENERATED ALWAYS AS (('https://i.pravatar.cc/128?u='::text || (email)::text)) STORED
);


ALTER TABLE public.users OWNER TO ybwkcejtdibexn;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: ybwkcejtdibexn
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO ybwkcejtdibexn;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ybwkcejtdibexn
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: v_users_attributes; Type: VIEW; Schema: public; Owner: ybwkcejtdibexn
--

CREATE VIEW public.v_users_attributes AS
 SELECT ua.id_user,
    ua.id_attribute,
    t.id AS id_type,
    a.name AS name_attribute,
    t.name AS name_type
   FROM ((public.user_attributes ua
     JOIN public.attributes a ON ((a.id = ua.id_attribute)))
     JOIN public.attribute_type t ON ((t.id = a.id_type)));


ALTER TABLE public.v_users_attributes OWNER TO ybwkcejtdibexn;

--
-- Name: attribute_type id; Type: DEFAULT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.attribute_type ALTER COLUMN id SET DEFAULT nextval('public.attribute_type_id_seq'::regclass);


--
-- Name: attributes id; Type: DEFAULT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.attributes ALTER COLUMN id SET DEFAULT nextval('public.attribute_list_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: attribute_type; Type: TABLE DATA; Schema: public; Owner: ybwkcejtdibexn
--

INSERT INTO public.attribute_type (id, name) VALUES (3, 'Movies Genres');
INSERT INTO public.attribute_type (id, name) VALUES (2, 'Music Genres');
INSERT INTO public.attribute_type (id, name) VALUES (1, 'Hobby');


--
-- Data for Name: attributes; Type: TABLE DATA; Schema: public; Owner: ybwkcejtdibexn
--

INSERT INTO public.attributes (id, id_type, name) VALUES (7, 3, 'Sci-fi');
INSERT INTO public.attributes (id, id_type, name) VALUES (8, 3, 'Action');
INSERT INTO public.attributes (id, id_type, name) VALUES (9, 3, 'Horror');
INSERT INTO public.attributes (id, id_type, name) VALUES (4, 2, 'Pop');
INSERT INTO public.attributes (id, id_type, name) VALUES (5, 2, 'Jazz');
INSERT INTO public.attributes (id, id_type, name) VALUES (6, 2, 'Rock');
INSERT INTO public.attributes (id, id_type, name) VALUES (1, 1, 'Sport');
INSERT INTO public.attributes (id, id_type, name) VALUES (2, 1, 'Football');
INSERT INTO public.attributes (id, id_type, name) VALUES (3, 1, 'Dancing');


--
-- Data for Name: user_attributes; Type: TABLE DATA; Schema: public; Owner: ybwkcejtdibexn
--

INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (1, 1);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (2, 9);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (1, 8);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (2, 2);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (1, 7);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (2, 3);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (1, 4);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (2, 6);
INSERT INTO public.user_attributes (id_user, id_attribute) VALUES (1, 5);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: ybwkcejtdibexn
--

INSERT INTO public.users (id, name, surname, email, password, avatar_url) VALUES (1, 'John', 'Snow', 'johnsnow@email.com', 'john123', DEFAULT);
INSERT INTO public.users (id, name, surname, email, password, avatar_url) VALUES (2, 'admin', 'admin', 'admin@admin.pl', 'admin123', DEFAULT);
INSERT INTO public.users (id, name, surname, email, password, avatar_url) VALUES (3, 'Imie', 'Nazwisko', 'asdasdas@gmail.com', '$2y$10$5voGpp1OMNFGGv6.Mvt/geqAg1UKW6nEzs8aLyOOsyg8k2nfXEfbe', DEFAULT);


--
-- Name: attribute_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ybwkcejtdibexn
--

SELECT pg_catalog.setval('public.attribute_list_id_seq', 9, true);


--
-- Name: attribute_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ybwkcejtdibexn
--

SELECT pg_catalog.setval('public.attribute_type_id_seq', 11, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ybwkcejtdibexn
--

SELECT pg_catalog.setval('public.users_id_seq', 3, true);


--
-- Name: attributes attribute_list_pk; Type: CONSTRAINT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.attributes
    ADD CONSTRAINT attribute_list_pk PRIMARY KEY (id);


--
-- Name: attribute_type attribute_type_pk; Type: CONSTRAINT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.attribute_type
    ADD CONSTRAINT attribute_type_pk PRIMARY KEY (id);


--
-- Name: user_attributes user_attributes_pk; Type: CONSTRAINT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.user_attributes
    ADD CONSTRAINT user_attributes_pk PRIMARY KEY (id_user, id_attribute);


--
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);


--
-- Name: attribute_list_name_uindex; Type: INDEX; Schema: public; Owner: ybwkcejtdibexn
--

CREATE UNIQUE INDEX attribute_list_name_uindex ON public.attributes USING btree (name);


--
-- Name: users_id_uindex; Type: INDEX; Schema: public; Owner: ybwkcejtdibexn
--

CREATE UNIQUE INDEX users_id_uindex ON public.users USING btree (id);


--
-- Name: attributes attribute_list_attribute_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.attributes
    ADD CONSTRAINT attribute_list_attribute_type_id_fk FOREIGN KEY (id_type) REFERENCES public.attribute_type(id) ON UPDATE CASCADE;


--
-- Name: user_attributes user_attributes_attribute_list_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.user_attributes
    ADD CONSTRAINT user_attributes_attribute_list_id_fk FOREIGN KEY (id_attribute) REFERENCES public.attributes(id) ON UPDATE CASCADE;


--
-- Name: user_attributes user_attributes_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: ybwkcejtdibexn
--

ALTER TABLE ONLY public.user_attributes
    ADD CONSTRAINT user_attributes_users_id_fk FOREIGN KEY (id_user) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: ybwkcejtdibexn
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO ybwkcejtdibexn;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO ybwkcejtdibexn;


--
-- PostgreSQL database dump complete
--

