<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>REGISTER</title>
    <script defer src="/public/js/register.js"></script>
</head>

<body>
<div class="logo-container">
    <img class="logo-img" src="public/img/logo.png" alt="">
</div>
<div class="form-container">
    <div class="login-form">
        <div>
            <h2>Join Us now!</h2>
            <h3>
                <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>
            </h3>
        </div>
        <form action="register" method="POST">
            <div class="input-part">
                <input class="input-login" name="email" type="text" placeholder="email@email.com">
                <input class="input-login" name="password" type="password" placeholder="password">
                <input class="input-login" name="confirmedPassword" type="password" placeholder="confirm password">
                <input class="input-login" name="name" type="text" placeholder="name">
                <input class="input-login" name="surname" type="text" placeholder="surname">
            </div>
            <div class="button-part">
                <button class="button-login" type="submit">Register</button>
                <a href="login.php"> <button class="button-login" type="button">Already got an account?</button> <a/>
            </div>
        </form>
    </div>
</div>
</body>
</html>