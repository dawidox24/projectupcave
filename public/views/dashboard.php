<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/dashboard.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700;800;900&display=swap" rel="stylesheet">
    <title>DASHBOARD</title>
</head>

<body>
    <div class="container">
        <div class="left-part">
            <div class="logo">
                <img class="logo-img" src="public/img/logo.png" alt="">
            </div>
            <div class="caption">
                <h1>Your friends</h1>
            </div>
            <div class="friends">
                <table class="table">
                    <tbody>
                    <tr>
                        <td><img src="public/img/avatar.png" alt="Avatar" class="avatar"></td>
                        <td><img src="public/img/avatar.png" alt="Avatar" class="avatar"></td>
                    </tr>
                    <tr>
                        <td><img src="public/img/avatar.png" alt="Avatar" class="avatar"></td>
                        <td><img src="public/img/avatar.png" alt="Avatar" class="avatar"></td>
                    </tr>
                    <tr>
                        <td><img src="public/img/avatar.png" alt="Avatar" class="avatar"></td>
                        <td><img src="public/img/avatar.png" alt="Avatar" class="avatar"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="button-show">
                <button>Show all friends</button>
            </div>
        </div>
        <div class="right-part">
            <?php include('nav.php') ?>
            <div class="container2">
                <div class="meetups">
                    <h1>Tu będzie mini-zakładka "meetups"</h1>
                </div>
                <div class="chat">
                    <h1>Tu będzie mini czat</h1>
                </div>
            </div>
        </div>
    </div>
</body>
</html>