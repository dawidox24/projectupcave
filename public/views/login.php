<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>LOGIN</title>
</head>

<body>
    <div class="logo-container">
        <img class="logo-img" src="public/img/logo.png" alt="">
    </div>
    <div class="form-container">
        <div class="login-form">
            <div>
                <h2>Hello there!</h2>
                <h3>
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
                </h3>
            </div>
            <form action="login" method="POST">
                <div class="input-part">
                    <label>
                        <input class="input-login" name="email" placeholder="login" type="text">
                    </label>
                    <label>
                        <input class="input-login" name="password" placeholder="password" type="password">
                    </label>
                </div>
                <div class="button-part">
                    <button class="button-login" type="submit">LOGIN</button>
                    <a href="register"> <button class="button-login" type="button">SING-UP</button> </a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>