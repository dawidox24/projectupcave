<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="public/css/dashboard.css">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700;800;900&display=swap" rel="stylesheet">
  <title>CHAT</title>
</head>

<body>
<div class="container">
  <div class="left-part">
    <div class="logo">
      <img class="logo-img" src="public/img/logo.png" alt="">
    </div>
    <div class="caption">
      <h1>Your friends</h1>
    </div>
    <div class="friends">
        <?php include('leftPanel.php') ?>
    </div>
    <div class="button-show">
      <button>Show all friends</button>
    </div>
  </div>
  <div class="right-part">
      <?php include('nav.php') ?>
    <div class="container2">
      <div class="meetups">
        <h1>szczegóły konta</h1>
      </div>
      <div class="chat">
        <h1>My account</h1>
      </div>
    </div>
  </div>
</div>
</body>
</html>