<div class="nav">
    <div class="buttons">
        <a href="dashboard"> <button class="style-4">Dashboard ⚡</button></a>
        <a href="chat"><button class="style-4">Chat ⚡</button></a>
        <a href="my_friends"><button class="style-4">My friends</button></a>
        <a href="meetups"><button class="style-4">Meetups</button></a>
        <a href="my_account"><button class="style-4">My account</button></a>
        <a href="logout"><button class="style-4">Logout</button></a>
    </div>
</div>
