<?php

require_once 'AppController.php';

class DefaultController extends AppController {

    public function index()
    {
        if(isset($_SESSION["u"])){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/dashboard");
        }else{
            $this->render('login');
        }
    }

    public function dashboard()
    {
        if(isset($_SESSION["u"])){
            $this->render('dashboard');
        }else{
            $this->render('login', ['messages' => ['To access dashboard you need to log in first!!!']]);
        }

    }

    public function chat()
    {
        if(isset($_SESSION["u"])){
            $this->render('chat');
        }else{
            $this->render('login', ['messages' => ['To access chat you need to log in first!']]);
        }
    }

    public function my_friends()
    {
        if(isset($_SESSION["u"])){
            $this->render('my_friends');
        }else{
            $this->render('login', ['messages' => ['To access your friends list you need to log in first!']]);
        }
    }

    public function meetups()
    {
        if(isset($_SESSION["u"])){
            $this->render('meetups');
        }else{
            $this->render('login', ['messages' => ['To access meetups page you need to log in first!']]);
        }
    }
    public function my_account()
    {
        if(isset($_SESSION["u"])){
            $this->render('my_account');
        }else{
            $this->render('login', ['messages' => ['To access your account details you need to log in first!']]);
        }
    }

    public function logout()
    {
        $_SESSION = null;
        session_abort();
        //session_destroy();
        return $this->render('login', ['messages' => ['You are logged out!']]);
    }
}