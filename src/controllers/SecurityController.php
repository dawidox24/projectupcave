<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../repository/UserRepository.php';

class SecurityController extends AppController{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login()
    {
        if (!$this->isPost()) {
            return $this->render('login');
        }
        if ($this->isPost()) {

            $userRepository = new UserRepository();

            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($email);

            if (!$user) {
                return $this->render('login', ['messages' => ['User not found!']]);
            }

            if ($user->getEmail() !== $email) {
                return $this->render('login', ['messages' => ['User with this email not exist!']]);
            }

            if ($user->getPassword() !== $password) {
                return $this->render('login', ['messages' => ['Wrong password!']]);
            }

            $_SESSION["u"] = $user->getEmail();
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/dashboard");
        }
    }

    public function register()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }

        if ($this->isPost()) {

            $email = $_POST['email'];
            $password = $_POST['password'];
            $confirmedPassword = $_POST['confirmedPassword'];
            $name = $_POST['name'];
            $surname = $_POST['surname'];

            if ($password !== $confirmedPassword) {
                return $this->render('register', ['messages' => ['Please provide proper password']]);
            }

            $user = new User($email, password_hash($password, PASSWORD_BCRYPT), $name, $surname);

            try {
                $this->userRepository->addUser($user);
                return $this->render('login', ['success' => ['You can now log in']]);
            } catch (PDOException $exec) {
                return $this->render('register', ['messages' => ['User with this username or email already exists!']]);
            }
        }
    }
}