<?php

require_once 'Repository.php';


class AttributeRepository extends Repository
{

    public function AddUserAttribute($user_id, $attribute_id)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.users_attributes (id_user, id_attribute) VALUES (:id_user, :id_attribute);
        ');

        $stmt->bindParam(':id_user', $user_id, PDO::PARAM_INT);
        $stmt->bindParam(':id_attribute', $attribute_id, PDO::PARAM_INT);

        $stmt->execute();
    }

    public function RemoveUserAttribute($user_id, $attribute_id)
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM public.sers_attributes WHERE id_user = :id_user AND id_attribute = :id_attribute;
        ');
        $stmt->bindParam(':id_user', $user_id, PDO::PARAM_INT);
        $stmt->bindParam(':id_attribute', $attribute_id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function GetUserAttributes($user_id): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.v_users_attributes WHERE id_user = :id_user;
        ');
        $stmt->bindParam(':id_user', $user_id, PDO::PARAM_INT);
        $stmt->execute();

        $user_attributes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($user_attributes == false) {
            return [];
        }

        return $user_attributes;
    }

    public function GetAttributes(): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.attributes;
        ');
        $stmt->execute();

        $attributes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($attributes == false) {
            return [];
        }

        return $attributes;
    }

    public function GetAttributeTypes(): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.attribute_type;
        ');

        $stmt->execute();

        $attribute_types = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($attribute_types == false) {
            return [];
        }

        return $attribute_types;
    }
}